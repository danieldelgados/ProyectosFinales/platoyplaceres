window.addEventListener("load",()=>{
	var fotos=['g1','g2','g3','g4','g5','g6','g7','g8','g9','g10','g11','g12','g14','g15','g16','g17','g18','g19','g20','g21','g22','g23','g24','g25','g26','g27',
	'g28','g29','g30','g31','g32','g33','g34','g35','g36'];
	
	var imagenes=[];
	var aux=0;

	for(var c=0;c<fotos.length;c++){
		imagenes[c]=new Image();
		imagenes[c].src="imgs/"+fotos[c]+".jpg";
	}
	
	fotos.forEach((valor,indice) => {
		//creo los divs y los a por cada elemento del array
		var cuadros=document.createElement("DIV"); 
		var fotos=document.createElement("A");
		var img=document.createElement("IMG");
		
		// asigno la clase a los divs para que coja estilos 
		cuadros.className='thumbnail';
		
		
		// asigno los divs creados al padre que le corresponda
		document.querySelector('.galeria').appendChild(cuadros);
		
		//asignar atributos a los 'a' e 'i' creados 
		// fotos.setAttribute("href","#");
		img.setAttribute("data-indice",indice);
		img.setAttribute("src","imgs/"+valor+".jpg");

		//asigno la clase para a las etiquetas img 
		img.className='foto';
		

		//asigno las etiquetas a a su padre
		cuadros.appendChild(fotos);
		fotos.appendChild(img);
		
	});	

	//al hacer click en las fotos de la galeria despliega la ventana modal casera
	
	
	for(var c=0;c<fotos.length;c++){	
		
		if(c==0){
			var slideActive=document.createElement("DIV");
			var imagenActive=document.createElement("IMG");
	
			slideActive.className='carousel-item active';
			imagenActive.className='d-block w-100';
	
			document.querySelector('.carousel-inner').appendChild(slideActive);	
			slideActive.appendChild(imagenActive);
	
			imagenActive.setAttribute("src","imgs/home1.jpg");
			imagenActive.setAttribute("alt","platoyplaceres");
		}

		var slideNormal=document.createElement("DIV");		
		var imagenNormal=document.createElement("IMG");		
		
		slideNormal.className='carousel-item';		
		imagenNormal.className='d-block w-100';
		
		document.querySelector('.carousel-inner').appendChild(slideNormal);			
		slideNormal.appendChild(imagenNormal);

		imagenNormal.setAttribute("src","imgs/"+fotos[c]+".jpg");
		imagenNormal.setAttribute("alt","platoyplaceres"+c);
		
	}
	
});


  
    
  